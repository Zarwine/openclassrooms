window.onload = function(){
    var mymap = L.map('map').setView([43.300047, 5.357829], 12);
    var tileStreets = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    accessToken: 'pk.eyJ1IjoiYW5nZTU1MCIsImEiOiJjazVtY3JucTkwdmZ2M2xvNHVsZXhocTNqIn0.nYzlMEwRxZ5Cvdu6EqMBQQ'
    }).addTo(mymap);

    tileStreets.addTo(mymap);
    var marker = L.marker([43.300047, 5.357829]).addTo(mymap);
    marker.bindPopup("<b>Hello world!</b><br>I am a popup.");

}
//https://api.jcdecaux.com/vls/v1/stations?contract=Marseille&apiKey=2c392e473ddfa9e2a6d03a6e4092df542163e305