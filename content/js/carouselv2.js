
class CarouselClass
{
    constructor(carouselID, item, nextButton, prevButton, pauseButton, playButton, lecteur)
    {   
        this.carousel = document.getElementById(carouselID),
        this.item =  this.carousel.getElementsByClassName(item),
        this.nextButton = this.createDivWithClass(nextButton),
        this.prevButton = this.createDivWithClass(prevButton),
        this.pauseButton = this.createDivWithClass(pauseButton),
        this.playButton = this.createDivWithClass(playButton),
        this.lecteur = this.createDivWithClass(lecteur)

        this.carousel.appendChild(this.lecteur)
        this.prevButton.innerHTML = '<i class="fas fa-chevron-left"></i>'
        this.nextButton.innerHTML = '<i class="fas fa-chevron-right"></i>' 
        this.carousel.appendChild(this.nextButton)
        this.carousel.appendChild(this.prevButton)           
        this.pauseButton.innerHTML = '<i class="fas fa-pause"></i>'
        this.playButton.innerHTML = '<i class="fas fa-play"></i>' 
        this.lecteur.appendChild(this.pauseButton)
        this.lecteur.appendChild(this.playButton)  

        this.totalSlide = this.item.length -1
        this.index = 0
        this.currentItem = this.item[this.index]
        this.timer = null

        this.lecture()
        
        this.nextButton.addEventListener('click', e => {
            this.diapo_suivant()
        })
        this.prevButton.addEventListener('click', e => {
            this.diapo_precedent()
        })
        window.addEventListener('keyup', e => {
            if (e.key === 'ArrowRight' || e.key ==='Right') {                
                this.diapo_suivant()                           
            } else if (e.key === 'ArrowLeft' || e.key ==='Left') {
                this.diapo_precedent()            
            }
        })
        this.playButton.addEventListener('click', e => {
            this.lecture()
        })
        this.pauseButton.addEventListener('click', e => {
            this.pause()
        })      
    }

    diapo_suivant()
    {        
        let currentItem_verification = this.currentItem.classList.contains("visible")
        if (currentItem_verification === true) {
            this.currentItem.classList.replace("visible", "not_visible")
        }
            this.currentItem = this.item[this.index] 
            this.index++
            this.currentItem.classList.replace("not_visible", "visible")

        if (this.index > this.totalSlide) {
            this.index = 0
            } 
        this.pause()  
    }
    diapo_precedent()
    {
        let currentItem_verification = this.currentItem.classList.contains("visible")
        if (currentItem_verification === true) {
            this.currentItem.classList.replace("visible", "not_visible")
        }  
            this.currentItem = this.item[this.index] 
            this.index--
            this.currentItem.classList.replace("not_visible", "visible")                    

        if (this.index < 0) {
            this.index = this.totalSlide
            } 
        this.pause()
    }
    lecture()
    {
        clearInterval(this.timer);  
        this.timer = setInterval( e =>
            {
                {    
                    let currentItem_verification = this.currentItem.classList.contains("visible")
                    if (currentItem_verification === true) {
                        this.currentItem.classList.replace("visible", "not_visible")
                    }
                        this.currentItem = this.item[this.index] 
                        this.index++
                        this.currentItem.classList.replace("not_visible", "visible")
    
                    if (this.index > this.totalSlide) {
                        this.index = 0
                    } 
                }
            }, 5000)
            this.playButton.classList.add("carousel_play_active")
            this.pauseButton.classList.remove("carousel_pause_active")
    }
    pause()
    {
        clearInterval(this.timer);    
        this.playButton.classList.remove("carousel_play_active")
        this.pauseButton.classList.add("carousel_pause_active")
    }
    createDivWithClass(className) 
    {
        let div = document.createElement('div')
        div.setAttribute('class', className)
        return div
    }
 
}

let slider = new CarouselClass ('carousel1', 'item', 'carousel_next', 'carousel_prev', 'carousel_pause', 'carousel_play', 'lecteur')
